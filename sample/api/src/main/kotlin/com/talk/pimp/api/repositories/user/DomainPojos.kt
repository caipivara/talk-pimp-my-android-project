package com.talk.pimp.api.repositories.user

data class User(val id: Long, val firstName: String, val email: String)