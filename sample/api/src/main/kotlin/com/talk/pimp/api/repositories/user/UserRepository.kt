package com.talk.pimp.api.repositories.user

import com.talk.pimp.api.Answer
import rx.Observable
import rx.Observable.just

open class UserRepository() {
  open fun get(): Observable<Answer<User>> = just(Answer(User(123, "Clark", "clark@superman.com")))
}


