package com.talk.pimp

import com.crashlytics.android.Crashlytics
import com.talk.pimp.BuildConfig
import com.talk.pimp.api.MyApi
import com.talk.pimp.api.utils.MyApiLog
import com.talk.pimp.base.BaseApp
import io.fabric.sdk.android.Fabric
import timber.log.Timber

/**
 * Debug version
 */
class MyApp : BaseApp() {

  companion object {
    //JUST FOR THIS BUILD TYPE
    lateinit var INSTANCE: MyApp
  }

  override fun onCreate() {
    super.onCreate()

    INSTANCE = this

    val logLevel = if (BuildConfig.DEBUG) MyApiLog.LEVEL_FULL else MyApiLog.LEVEL_BASIC
    MyApi.init("http://my-api.com", logLevel)
//        StethoInterceptor()) if you have stetho
  }

}