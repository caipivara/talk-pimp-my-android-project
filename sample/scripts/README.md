# Scripts

App should be able to be built/tested/released by using just _gradle_ tasks but may be hard to
remember which ones for what. These scripts group those tasks for easy access and management.

*Note*: For _ci_ you should create a variable `IS_CI_SERVER=true` to avoid using gradle daemon and
other things there.

## Env Variables

- IS_CI_SERVER
- JAVA7_HOME
- ANDROID_HOME (needs to be in path)

## Definition

// TODO: Fix CI tests
- `test.sh`: run to start unit testing
- `build-*.sh`: run to create apk files on `app/build/output/apk/*.apk`
- `ship-*.sh`: run to release the app into _Fabric_.