#!/usr/bin/env bash
#
# Assemble and release dev staging apk to fabrid.
#
: ${GRADLEW:=gradlew}
: ${APP_FOLDER:=app}

LOCAL_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ -z $IS_CI_SERVER ] ; then
  # If IS_CI_SERVER is not set...
  ./$GRADLEW assembleRelease --daemon --parallel
else
  ./$GRADLEW assembleRelease --stacktrace -PdisablePreDex --no-daemon -Dscan
fi